package br.com.proway.prova.interfaces;

import br.com.proway.prova.models.Produto;

import java.util.ArrayList;

public interface IFuncionario {
    void adicionarProdutoEstoque(ArrayList<Produto> listaProdutosEstoque, Produto produto);
}
