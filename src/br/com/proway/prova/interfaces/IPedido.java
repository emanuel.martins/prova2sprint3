package br.com.proway.prova.interfaces;

import br.com.proway.prova.models.Produto;

public interface IPedido {
    void adicionarProdutoCarrinho(Produto produto);
    void removerProdutoCarrinho(int produtoID);
}
