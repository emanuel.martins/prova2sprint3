package br.com.proway.prova.enums;

public enum Tamanho {
    TAMANHO_PP("PP"),
    TAMANHO_P("P"),
    TAMANHO_M("M"),
    TAMANHO_G("G"),
    TAMANHO_GG("GG"),
    ;

    private final String texto;

    Tamanho(String texto) {
        this.texto = texto;
    }

    public String getTexto() {
        return texto;
    }
}
