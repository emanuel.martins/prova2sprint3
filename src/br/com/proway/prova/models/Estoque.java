package br.com.proway.prova.models;

import java.util.ArrayList;

public class Estoque {
    private ArrayList<Produto> roupasVenda = new ArrayList<>();

    public ArrayList<Produto> getRoupasVenda() {
        return roupasVenda;
    }

    public void setRoupasVenda(ArrayList<Produto> roupasVenda) {
        this.roupasVenda = roupasVenda;
    }
}
