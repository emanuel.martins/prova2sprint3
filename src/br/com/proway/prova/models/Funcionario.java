package br.com.proway.prova.models;

import br.com.proway.prova.enums.Cargo;
import br.com.proway.prova.interfaces.IFuncionario;

import java.util.ArrayList;

public class Funcionario extends Usuario implements IFuncionario {
    private String numeroRegistroLoja;
    private Cargo cargo;

    public Funcionario(Integer usuarioId, String nome, Integer idade, String email, String numeroRegistroLoja, Cargo cargo) {
        super(usuarioId, nome, idade, email);
        this.numeroRegistroLoja = numeroRegistroLoja;
        this.cargo = cargo;
    }

    public String getNumeroRegistroLoja() {
        return numeroRegistroLoja;
    }

    public void setNumeroRegistroLoja(String numeroRegistroLoja) {
        this.numeroRegistroLoja = numeroRegistroLoja;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    @Override
    public void adicionarProdutoEstoque(ArrayList<Produto> listaProdutosEstoque, Produto produto) {
        listaProdutosEstoque.add(produto);
    }
}
