package br.com.proway.prova.models;

import br.com.proway.prova.interfaces.IPedido;

import java.util.ArrayList;

public class Cliente extends Usuario implements IPedido {
    private String numeroCartaoLoja;
    private String validadeCartao;
    private Integer codigoSeguranca;
    private Carrinho carrinho = new Carrinho(new ArrayList<>());

    public Cliente(Integer usuarioId, String nome, Integer idade, String email) {
        super(usuarioId, nome, idade, email);
    }

    public String getNumeroCartaoLoja() {
        return numeroCartaoLoja;
    }

    public void setNumeroCartaoLoja(String numeroCartaoLoja) {
        this.numeroCartaoLoja = numeroCartaoLoja;
    }

    public String getValidadeCartao() {
        return validadeCartao;
    }

    public void setValidadeCartao(String validadeCartao) {
        this.validadeCartao = validadeCartao;
    }

    public Integer getCodigoSeguranca() {
        return codigoSeguranca;
    }

    public void setCodigoSeguranca(Integer codigoSeguranca) {
        this.codigoSeguranca = codigoSeguranca;
    }

    public Carrinho getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(Carrinho carrinho) {
        this.carrinho = carrinho;
    }

    @Override
    public void adicionarProdutoCarrinho(Produto produto) {
        carrinho.getCarrinho().add(produto);
    }

    @Override
    public void removerProdutoCarrinho(int produtoID) {
        carrinho.getCarrinho().removeIf(produtoParam -> produtoParam.getProdutoId() == produtoID);
    }
}
