package br.com.proway.prova.models;

public abstract class Produto {
    private Integer produtoId;
    private String tipo;
    private String descricao;

    public Produto(Integer produtoId, String tipo, String descricao) {
        this.produtoId = produtoId;
        this.tipo = tipo;
        this.descricao = descricao;
    }

    public Integer getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
