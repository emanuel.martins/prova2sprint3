package br.com.proway.prova.models;

import br.com.proway.prova.enums.Tamanho;

public class Roupa extends Produto {
    private Tamanho tamanho;
    private Double preco;

    public Roupa(Integer produtoId, String tipo, String descricao, Tamanho tamanho, Double preco) {
        super(produtoId, tipo, descricao);
        this.tamanho = tamanho;
        this.preco = preco;
    }

    public Tamanho getTamanho() {
        return tamanho;
    }

    public void setTamanho(Tamanho tamanho) {
        this.tamanho = tamanho;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
}
