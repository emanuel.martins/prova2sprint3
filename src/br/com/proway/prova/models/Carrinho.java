package br.com.proway.prova.models;

import java.util.ArrayList;

public class Carrinho {
    private ArrayList<Produto> carrinho;

    public Carrinho(ArrayList<Produto> carrinho) {
        this.carrinho = carrinho;
    }

    public ArrayList<Produto> getCarrinho() {
        return carrinho;
    }

    public void setCarrinho(ArrayList<Produto> carrinho) {
        this.carrinho = carrinho;
    }
}
