package br.com.proway.prova.models;

public abstract class Usuario {
    private Integer usuarioId;
    private String nome;
    private Integer idade;
    private String email;

    public Usuario(Integer usuarioId, String nome, Integer idade, String email) {
        this.usuarioId = usuarioId;
        this.nome = nome;
        this.idade = idade;
        this.email = email;
    }

    public Integer getPessoaId() {
        return usuarioId;
    }

    public void setPessoaId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getIdade() {
        return idade;
    }

    public void setIdade(Integer idade) {
        this.idade = idade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
