package br.com.proway.prova;

import br.com.proway.prova.enums.Cargo;
import br.com.proway.prova.enums.Tamanho;
import br.com.proway.prova.models.Cliente;
import br.com.proway.prova.models.Estoque;
import br.com.proway.prova.models.Funcionario;
import br.com.proway.prova.models.Roupa;

import javax.swing.*;

public class IniciarAplicacao {
    Cliente cliente = new Cliente(1, "Cliente 01", 20, "cliente@mail.com");
    Funcionario funcionario = new Funcionario(2, "Funcionario 01", 23,
            "funcionario@loja.com", "0001", Cargo.ESTOQUISTA);
    Estoque estoqueLoja = new Estoque();

    public void comecar() {
        String textoApresentacao;

        inserirProdutosVitrine(5, "Camiseta", "Camiseta Branca Masc."
                , Tamanho.TAMANHO_G, 45.0);
        inserirProdutosVitrine(5, "Camiseta", "Camiseta Preta Fem."
                , Tamanho.TAMANHO_M, 42.0);
        inserirProdutosVitrine(5, "Camiseta", "Camiseta Roxa Unisex."
                , Tamanho.TAMANHO_M, 50.0);

        try {
            while(true) {
                String[] opcoesIniciais = {"Ver Roupas", "Funcionario", "Sair"};
                textoApresentacao = "   Lojas de Roupa\n";

                int escolhaUsuario = JOptionPane.showOptionDialog(null
                        , textoApresentacao, "Bem-vindo"
                        , JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE
                        , null, opcoesIniciais, opcoesIniciais[0]);

                if (escolhaUsuario == 0) {
                    String textoRoupas = mostrarTextoRoupas();
                    String[] opcoesCliente = {"Adicionar ao Carrinho", "Ver Carrinho", "Remover do Carrinho", "Voltar"};

                    int escolhaCliente = JOptionPane.showOptionDialog(null,
                            textoRoupas, "Vitrine", JOptionPane.DEFAULT_OPTION , JOptionPane.INFORMATION_MESSAGE
                            , null, opcoesCliente, opcoesCliente[1]);

                    if (escolhaCliente == 0) {
                        try {
                            int escolhaRoupaCliente = Integer.parseInt((String) JOptionPane.showInputDialog(
                                    null
                                    , textoRoupas, "Escolha uma peça", JOptionPane.PLAIN_MESSAGE
                                    , null, null, ""));

                            Roupa roupaEscolhida = (Roupa) estoqueLoja.getRoupasVenda().stream()
                                    .filter(param -> param.getProdutoId() == escolhaRoupaCliente)
                                    .findFirst().get();

                            cliente.adicionarProdutoCarrinho(roupaEscolhida);
                            estoqueLoja.getRoupasVenda().removeIf(roupaParam ->
                                    roupaParam.getProdutoId() == roupaEscolhida.getProdutoId());
                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, "Verifique a operação e, \n" +
                                    "por favor digite apenas o número do produto");
                            System.out.println("[ERRO]: " + e.getMessage());
                        }
                    }

                    if (escolhaCliente == 1) {
                        String[] opcaoVoltar = {"Voltar"};

                        JOptionPane.showOptionDialog(null
                                , mostrarProdutosCarrinho(), "Carrinho", JOptionPane.DEFAULT_OPTION
                                , JOptionPane.INFORMATION_MESSAGE, null, opcaoVoltar, opcaoVoltar[0]);
                    }

                    if (escolhaCliente == 2) {
                        try {
                            int escolhaRemoverCarrinho = Integer.parseInt((String) JOptionPane.showInputDialog(
                                    null
                                    , mostrarProdutosCarrinho(), "Escolha uma peça", JOptionPane.PLAIN_MESSAGE
                                    , null, null, ""));

                            Roupa roupaEscolhida = (Roupa) cliente.getCarrinho().getCarrinho().stream()
                                    .filter(param -> param.getProdutoId() == escolhaRemoverCarrinho)
                                    .findFirst().get();

                            estoqueLoja.getRoupasVenda().add(roupaEscolhida);
                            cliente.removerProdutoCarrinho(roupaEscolhida.getProdutoId());
                        } catch (Exception e) {
                            System.out.println("[ERRO]: " + e.getMessage());
                            JOptionPane.showMessageDialog(null, "Verifique a operação e, \n" +
                                    "por favor escolha apenas um dos produtos no carrinho");
                        }
                    }

                    if (escolhaCliente == 3) {
                        continue;
                    }
                }

                if (escolhaUsuario == 1) {
                    JTextField registro = new JTextField();
                    JTextField email = new JTextField();

                    final JComponent[] credenciaisUsuario = new JComponent[] {
                            new JLabel("Email"),
                            email,
                            new JLabel("Número de Registro"),
                            registro
                    };

                    int resultado = JOptionPane.showConfirmDialog(null, credenciaisUsuario, "Login" +
                                    " Funcionário",
                            JOptionPane.PLAIN_MESSAGE);
                    if (resultado == JOptionPane.OK_OPTION) {
                        try {
                            if (autenticarUsuario(registro.getText(), email.getText())) {
                                String[] opcoesFuncionario = {"Ver Estoque", "Inserir Estoque", "Voltar"};

                                int escolhaFuncionario = JOptionPane.showOptionDialog(null
                                        , textoApresentacao, "Funcionários"
                                        , JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE
                                        , null, opcoesFuncionario, opcoesFuncionario[0]);

                                if (escolhaFuncionario == 0) {
                                    String textoProdutosEstoque = "";
                                    for (int i = 0; i < estoqueLoja.getRoupasVenda().size(); i++) {
                                        Roupa roupaEstoque = (Roupa) estoqueLoja.getRoupasVenda().get(i);

                                        textoProdutosEstoque += roupaEstoque.getProdutoId() + ": "
                                                + roupaEstoque.getDescricao()
                                                + ", Tamanho: " + roupaEstoque.getTamanho().getTexto() + "\n";
                                    }

                                    JOptionPane.showMessageDialog(null, textoProdutosEstoque);
                                }

                                if (escolhaFuncionario == 1) {
                                    try {
                                        if (funcionario.getCargo() != Cargo.ESTOQUISTA) {
                                            throw new Exception("Você não possui permissão para esta ação!");
                                        }

                                        JTextField tipo = new JTextField();
                                        JTextField descricao = new JTextField();
                                        JTextField preco = new JTextField();

                                        final JComponent[] inputs = new JComponent[] {
                                                new JLabel("Tipo"),
                                                tipo,
                                                new JLabel("Descrição"),
                                                descricao,
                                                new JLabel("Preço"),
                                                preco
                                        };

                                        int result = JOptionPane.showConfirmDialog(null, inputs
                                                , opcoesFuncionario[1], JOptionPane.PLAIN_MESSAGE);
                                        if (result == JOptionPane.OK_OPTION && !tipo.getText().isEmpty()
                                                && !descricao.getText().isEmpty()) {
                                            Tamanho tamanho = (Tamanho) JOptionPane.showInputDialog(null
                                                    , "Escolha", "Escolha o Tamanho da Peça"
                                                    , JOptionPane.PLAIN_MESSAGE, null, Tamanho.values()
                                                    , Tamanho.values()[0]);

                                            System.out.println("[ROUPA]: " +
                                                    tipo.getText() + ", " +
                                                    descricao.getText() + ", " +
                                                    tamanho.getTexto());

                                            funcionario.adicionarProdutoEstoque(estoqueLoja.getRoupasVenda(), new Roupa(
                                                    estoqueLoja.getRoupasVenda().size() + 1,
                                                    tipo.getText(), descricao.getText(), tamanho,
                                                    Double.parseDouble(preco.getText())
                                            ));
                                        }
                                    } catch (Exception e) {
                                        System.out.println("[403 FORBIDDEN]: Funcionário "
                                                + funcionario.getNumeroRegistroLoja() + " não possui acesso ao"
                                                + " recurso " + opcoesFuncionario[1]);
                                        JOptionPane.showMessageDialog(null, e.getMessage());
                                    }
                                }

                                if (escolhaFuncionario == 2) {
                                    continue;
                                }
                            }
                        } catch (Exception e) {
                            System.out.println("[ERRO]: " + e.getMessage());
                            JOptionPane.showMessageDialog(null, "Credenciais inválidas!");
                        }
                    }
                }

                if (escolhaUsuario == 2) {
                    break;
                }
            }
        } catch (Exception e) {
            System.out.println("[ERRO]: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Erro no Sistema\nComunique o suporte técnico!");
        }
    }

    private void inserirProdutosVitrine(int quantidade, String tipo, String descricao, Tamanho tamanho, Double preco) {
        int totalEstoque = estoqueLoja.getRoupasVenda().size();

        for (int i = 0; i < quantidade; i++) {
            estoqueLoja.getRoupasVenda().add(new Roupa(totalEstoque + i + 1, tipo, descricao,
                    tamanho, preco));
        }
    }

    private String mostrarTextoRoupas() {
        String mostrarRoupas = "";

        for (int i = 0; i < estoqueLoja.getRoupasVenda().size(); i++) {
            Roupa roupa = (Roupa) estoqueLoja.getRoupasVenda().get(i);
            mostrarRoupas += roupa.getProdutoId() + ": " + roupa.getDescricao() + ", Tamanho: "
                    + roupa.getTamanho().getTexto()
                    + "\n" + "Preço: R$" + roupa.getPreco() + "\n";
        }

        return mostrarRoupas;
    }

    private String mostrarProdutosCarrinho() {
        String produtosCarrinho = "";

        for (int i = 0; i < cliente.getCarrinho().getCarrinho().size(); i++) {
            Roupa produtoCarrinhocliente = (Roupa) cliente.getCarrinho().getCarrinho().get(i);

            produtosCarrinho += produtoCarrinhocliente.getProdutoId() + ": " + produtoCarrinhocliente.getDescricao()
                 + ", Tamanho: " + produtoCarrinhocliente.getTamanho().getTexto() + "\n";
        }

        return produtosCarrinho;
    }

    public boolean autenticarUsuario(String registro, String email) {
        if (!funcionario.getEmail().equals(email)) {
            return false;
        }

        if (!funcionario.getNumeroRegistroLoja().equals(registro)) {
            return false;
        }

        return true;
    }
}
